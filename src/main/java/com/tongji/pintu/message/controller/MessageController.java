package com.tongji.pintu.message.controller;

import com.alibaba.fastjson.JSONObject;
import com.tongji.pintu.message.config.CosConfiguration;
import com.tongji.pintu.message.domain.Message;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@Api("消息模块")
@RestController
public class MessageController {
    public CosConfiguration cosConfiguration;
	static RestTemplate restTemplate = new RestTemplate();
	static String APPID = "wx1c133c39f443da81";
	static String APPSECRET = "0c5f354421c981f2eee3312c53cf2d4c";

	@GetMapping("/hello")
	@ApiOperation("HelloController")
	public Object showMessage(Message message){
		return "hello";
	}

	@GetMapping("/wechat/credentials")
	@ApiOperation("获取微信授权")
	public Object getCredentials(){
		String urlGetAccessToken = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=APPID&secret=APPSECRET";
		urlGetAccessToken = urlGetAccessToken.replace("APPID",APPID).replace("APPSECRET",APPSECRET);
		JSONObject res =restTemplate.getForObject(urlGetAccessToken, JSONObject.class);
		String ACCESS_TOKEN = res.getString("access_token");
		String urlGetJsapiToken = "https://api.weixin.qq.com/cgi-bin/ticket/getticket?access_token=ACCESS_TOKEN&type=jsapi";
		urlGetJsapiToken = urlGetJsapiToken.replace("ACCESS_TOKEN",ACCESS_TOKEN);
		res =restTemplate.getForObject(urlGetJsapiToken, JSONObject.class);
		return res;
    }

	@GetMapping("/tencent/credentials")
    @ApiOperation(value = "获取腾讯云COS密钥")
	public String getCredential(){
		return cosConfiguration.getCredential().toString();
	}
}
