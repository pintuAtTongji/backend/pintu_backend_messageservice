package com.tongji.pintu.message.domain;

import com.alibaba.fastjson.JSONObject;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.io.Console;
import java.nio.charset.StandardCharsets;
import java.util.Date;

@Setter
@Getter
@ApiModel
public class Message {
	@ApiModelProperty(value = "发送者Id", notes = "0表示系统推送")
	public Integer senderId;
	@ApiModelProperty("接收者Id")
	public Integer receiverId;
	@ApiModelProperty("发送者账号名称")
	public String senderName;
	@ApiModelProperty("发送者账号头像url")
	public String senderAvatarUrl;
	@ApiModelProperty("消息内容")
	public String content;
	@ApiModelProperty(value = "时间",hidden = true)
	public Date time;
	@ApiModelProperty(value = "消息类型", example = "SYSTEM/PERSONAL")
	public String type;

	private static RestTemplate restTemplate = new RestTemplate();

	public Message(){}

	public Message(Integer senderId, Integer receiverId, String senderName, String senderAvatarUrl, String content, Date time, String type) {
		this.senderId = senderId;
		this.receiverId = receiverId;
		this.senderName = senderName;
		this.senderAvatarUrl = senderAvatarUrl;
		this.content = content;
		this.time = time;
		this.type = type;
		this.fullfill();
	}

	public void fullfill(){
		//restTemplate get senderInfo
		if(this.senderId!=null){
			String url = "http://121.199.79.177:11001/user/api/user/"+senderId+"/info";
			restTemplate.getMessageConverters().set(1, new StringHttpMessageConverter(StandardCharsets.UTF_8));
			String res = restTemplate.getForObject(url, String.class);
			JSONObject resJsonobj = JSONObject.parseObject(res).getJSONObject("data");
			if(resJsonobj!=null){
				this.senderName = resJsonobj.getString("name")!=null?resJsonobj.getString("name"):this.senderName;
				this.senderAvatarUrl = resJsonobj.getString("profilePhotoUrl")!=null?resJsonobj.getString("profilePhotoUrl"):this.senderAvatarUrl;
			}
		}
		//set system type
		if(this.type!=null && this.type.equals("SYSTEM")){
			this.senderId=0;
			this.senderName="拼图小助手";
			this.senderAvatarUrl="https://publicmessage-1259081301.cos.ap-shanghai.myqcloud.com/pintu/system/pintu%20helper.png";
		}
	}
}
