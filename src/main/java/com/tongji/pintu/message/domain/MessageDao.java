package com.tongji.pintu.message.domain;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
//@ApiModel
public class MessageDao {
	//@ApiModelProperty("接收者Id")
	public String receiverId;
	//@ApiModelProperty("消息其它部分内容")
	public String content;

	public String getReceiverId() {
		return receiverId;
	}

	public void setReceiverId(String receiverId) {
		this.receiverId = receiverId;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}
}
