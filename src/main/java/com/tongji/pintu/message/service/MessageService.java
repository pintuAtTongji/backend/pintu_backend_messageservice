package com.tongji.pintu.message.service;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.tongji.pintu.message.domain.Message;
import com.tongji.pintu.message.domain.MessageDao;
import com.tongji.pintu.message.util.MessageTemplateUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

@Component
public class MessageService {
	@Autowired
	private MessageTemplateUtil messageTemplateUtil;

	public static long EXPIRE_DAYS = 10;

	public String getUserMessage(Integer receiverId){
		List<Message> messages = messageTemplateUtil.get(receiverId.toString());
		return JSONArray.toJSONString(messages);
	}

	public Boolean postMessage(Message message){
		try{
			messageTemplateUtil.set(message.receiverId.toString(),message);
			return true;
		}catch (Exception e){
			return false;
		}
	}
}
