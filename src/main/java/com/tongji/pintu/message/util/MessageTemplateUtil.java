package com.tongji.pintu.message.util;

import com.tongji.pintu.message.domain.Message;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.List;

@Component
public class MessageTemplateUtil {
	@Autowired
	RedisTemplate<String, Message> messageTemplate;

	public List<Message> get(String key){
		//TODO 取得数据后是否要删除
		return key==null?null:messageTemplate.opsForList().range(key, 0, -1);
	}

	public boolean set(String key,Message value) {
		try {
			messageTemplate.opsForList().rightPush(key, value);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
}
