package com.tongji.pintu.message.websocket;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.tongji.pintu.message.config.WebSocketContextAware;
import com.tongji.pintu.message.domain.Message;
import com.tongji.pintu.message.domain.MessageDao;
import com.tongji.pintu.message.service.MessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.websocket.*;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;
import java.util.Date;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author chaoszh
 * @since 2019-12-13
 * */

@Component
@ServerEndpoint("/message/api/{userId}")
public class WebSocketServer {
	private static ConcurrentHashMap<Integer, WebSocketServer> webSocketMap = new ConcurrentHashMap<>();
	private MessageService messageService = (MessageService) WebSocketContextAware.getApplicationContext().getBean("messageService");
	private Integer userId;
	private Session session;

	/**
	 * OPEN LINK
	 * */
	@OnOpen
	public void onOpen(Session session, @PathParam("userId") Integer userId){
		//initialize
		this.userId=userId;
		this.session=session;
		if(webSocketMap.containsKey(userId)){
			webSocketMap.get(userId).onClose();
		}
		System.out.println("[SERVER LOG] "+userId+" connect.");
		//add to webSocketMap
		webSocketMap.put(userId, this);
		//messageService
		String leftMessages=messageService.getUserMessage(userId);
		if(leftMessages!=null){
			this.onSend(leftMessages);
		}
	}

	/**
	 * CLOSE LINK
	 * */
	@OnClose
	public void onClose() {
		System.out.println("[SERVER LOG] "+userId+" exit.");
		webSocketMap.remove(this.userId);
	}

	/**
	 * RECEIVE MESSAGE
	 * */
	@OnMessage
	public void onReceive(String message, Session session) {
		System.out.println("[USER->SERVER] "+message);
		this.feedBack(message);
		try{
			JSONObject messageJsonObj= JSON.parseObject(message);
			Integer receiverId = messageJsonObj.getInteger("receiverId");
			Message messageObj = new Message(
				this.userId,
				messageJsonObj.getInteger("receiverId"),
				messageJsonObj.getString("systemSender"),
				messageJsonObj.getString("systemSenderUrl"),
				messageJsonObj.getString("content"),
				new Date(System.currentTimeMillis()),
				messageJsonObj.getString("type")
			);
			//如果在线则推送回去
			if(webSocketMap.containsKey(receiverId)){
				JSONArray messageArr= new JSONArray();
				messageArr.add(messageObj);
				webSocketMap.get(receiverId).onSend(messageArr.toJSONString());
			}
			//如果不在线，存到数据库里
			messageService.postMessage(messageObj);
		}catch (Exception e){
			System.out.println(e);
			e.printStackTrace();
		}
	}

	/**
	 * SEND MESSAGE
	 * */
	public void onSend(String message){
		System.out.println("[SERVER->USER] "+message);
		try{
			this.session.getBasicRemote().sendText(message);
		}catch (Exception e){
			System.out.println(e);
		}
	}

	public void feedBack(String message){
		try{
			this.session.getBasicRemote().sendText("[Message Server] "+message);
		}catch (Exception e){
			System.out.println(e);
		}
	}

	@OnError
	public void onError(Session session, Throwable error) {
		System.out.println("[MessageService] OnError");
		error.printStackTrace();
		try{
			this.feedBack("On encountering a certain error, our session is closed. You should try to connect again.");
			session.close();
		}catch(Exception e){
			System.out.println(e);
			e.printStackTrace();
		}
	}
}
